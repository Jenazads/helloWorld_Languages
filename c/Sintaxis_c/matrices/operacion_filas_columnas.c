#include<stdio.h>  
int main(){
    int i,j,tabla[5][5],opcion,x,y;
    printf("Ingrese una tabla de 5x5: \n");
    for(i=0;i<5;i++) {
       for(j=0;j<5;j++)
          scanf("%d",&tabla[i][j]);
    }
      do {
    printf("Operación a realizar:\n 1.Intercambiar filas.\n 2.Intercambiar columnas.\n 3.Intercambiar fila y columna.\n");
    scanf("%d",&opcion);
    switch(opcion){
	case 1: printf("¿Qué filas desea cambiar?");
           	scanf("%d %d",&x,&y);
  		for(i=0;i<5;i++) {
                    for(j=0;j<5;j++){                 		
               		if(i==x-1)
               		   printf("%d ",tabla[y-1][j]);
	                else if(i==y-1)
           	           printf("%d ",tabla[x-1][j]);
	                else 
                	   printf("%d ",tabla[i][j]);       
      	 	    }
	            printf("\n");
		}   	
                break;
	case 2: printf("¿Qué columnas desea cambiar?");
           	scanf("%d %d",&x,&y);
     		for(i=0;i<5;i++) {
                    for(j=0;j<5;j++){                      		
            		if(j==x-1)
               		   printf("%d ",tabla[i][y-1]);
	            	else if(j==y-1)
	                   printf("%d ",tabla[i][x-1]);
            		else 
            		   printf("%d ",tabla[i][j]);
      		    }
	            printf("\n");
	        }		 
 		break;
	case 3: printf("¿Qué fila y columna desea cambiar?");
           	scanf("%d %d",&x,&y);
		for(i=0;i<5;i++) {
         	    for(j=0;j<5;j++){  		
		       if(i==x-1)
                	  printf("%d ",tabla[j][y-1]);
	               else if(j==y-1)
          	          printf("%d ",tabla[x-1][i]);
	               else 
          	          printf("%d ",tabla[i][j]);
         	    }
	            printf("\n");
	        }
 		break;
	default: printf("Esa opción no es válida.\n");
      }
    }while(opcion!=0);
    return 0;
}
