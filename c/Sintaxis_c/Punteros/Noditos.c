#include <stdio.h>
#include <stdlib.h>
typedef struct Nodo{
    int val;
    struct Nodo *sig;
}nodo;
int main(){
    int x;
    nodo *p=NULL;
    while((scanf("%d",&x)),x!=0){
        nodo *aux=malloc(sizeof(nodo));
        aux->val=x;                 // es vez de escribir *aux.val 
        aux->sig=p;
        p=aux;
    }
    for(;p!=NULL;p=p->sig)
        printf("%d",p->val);
    printf("\n");
return 0;
}
