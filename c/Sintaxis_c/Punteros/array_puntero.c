// equivalencia entre arreglos y punteros
#include<stdio.h>
int main(){
   int A[5]={ -15,18,25,37,52};
   int *ptr,i;  //declaracion de la variable puntero
   ptr=&A[0]; //apunta al primer elemento de A
   for(i=0;i<4;i++) {
       printf("A[%d] = %d \t",i,A[i]);
       printf("*(ptr+ %d)= %d \n",i,*(ptr+i));
    }
    return 0;
  } 
