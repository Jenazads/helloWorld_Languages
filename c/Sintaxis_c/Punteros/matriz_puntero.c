
//ordenamiento de una matriz de mayor a menor via punteros o bucles;
#include<stdio.h>
#include<stdlib.h>
void ordenamiento(int **matriz,int filas,int columnas)
{
 int i,j,x,y,n;
 for(i=0;i<filas;i++)
     for(j=0;j<columnas;j++)
         for(x=0;x<filas;x++)
            for(y=0;y<columnas;y++)
                if((*(*(matriz+i)+j)) >(*(*(matriz+x)+y)))
                  { n=(*(*(matriz+i)+j));
                    (*(*(matriz+i)+j))=(*(*(matriz+x)+y));
                   (*(*(matriz+x)+y))=n;
                   }
//Lo que hace este bucle es posicionarse en la primera posicion de la matriz en los primeros dos for ; leugo en los dos for que sigen tmabien van a la misma posicion pero con las variables x e y recorriendo toda la matriz buscando el mayor valor y intercambiandolo con el valor que este en la posicion i,j
}
int ** reservar_memoria(int filas,int columnas)
{
 int i ;
 int **matriz;
 matriz=(int **)malloc(filas*sizeof(int*));
 for(i=0;i<filas;i++)
    matriz[i]=(int *)malloc(columnas*sizeof(int));
return matriz;
}

void llenar_matriz(int **matriz,int filas,int columnas)
{
  int i,j;
 for(i=0;i<filas;i++)
     for(j=0;j<columnas;j++)
         scanf("%d",(*(matriz+i)+j));//esta operacion es valida ya que apunto ala direcion de memoria de *(matriz+i)+j  que no e slo mismo que *(*(matriz+i)+j) ya que este es dato y no posicion de memoria
}
void imprimir_matriz(int **matriz,int filas,int columnas)
{
  int i,j;
 for(i=0;i<filas;i++)
     {for(j=0;j<columnas;j++)
         printf("%d  ",(*(*(matriz+i)+j)));
          printf("\n");}
}
int main()
{int filas,columnas,i,j,x,y,n;
 int** matriz=NULL;
 printf("Ingrese las filas :");
 scanf("%d",&filas);
 printf("Ingrese el numero de columnas :");
 scanf("%d",&columnas);
 matriz=reservar_memoria(filas,columnas);
 llenar_matriz(matriz,filas,columnas);
 printf("Antes del ordenamiento :\n");
 imprimir_matriz(matriz,filas,columnas); 
/*for(i=0;i<filas;i++)                //descomentar para ordenar por bucles 
     for(j=0;j<columnas;j++)
         for(x=0;x<filas;x++)
            for(y=0;y<columnas;y++)
                if(matriz[i][j]>matriz[x][y])
                  { n=matriz[i][j];
                    matriz[i][j]=matriz[x][y];
                    matriz[x][y]=n;
 }   */
 ordenamiento(matriz,filas,columnas);
 printf("Luego del ordenamiento:\n");
 imprimir_matriz(matriz,filas,columnas);

free(matriz);

return 0;}
