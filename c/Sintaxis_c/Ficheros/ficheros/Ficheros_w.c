// escriba un programa para escribir una cadena en un fichero o archivo
#include <stdio.h>
int main(){
	char cadena[31];
	FILE *fich;      //declaracion de un fichero
	printf("Ingrese cadena:  "); gets(cadena);
	if( (fich=fopen("archivo.txt","w"))==NULL ){ // w es write (escribir y editarlo)
		printf("No se pudo crear fichero\n");
	return 0;
	}
	
	fputs(cadena,fich); //graba una cadena en un fichero
	fclose(fich); //cierra el fichero	
return 0;
}
