#include <stdio.h>
int main( )
{
   char str[100];
   int i;

   printf( "Enter a value :");
   scanf("%s %d", str, &i);

   printf( "\nYou entered: %s %d ", str, i);

   return 0;
}

/*

la funcion int scanf(const char *format,...)

lee la entrada del flujo de entrada estándar stdin y analiza esa entrada de acuerdo al formato proporcionado.

la funcion int printf(const char *format, ...)

Escribe la salida en la salida estándar flujo(stdout) de salida estándar y produce salida de acuerdo con un formato establecido.

*/
