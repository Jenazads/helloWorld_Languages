/*

register se usa para definir una variable LOCAL

las variables locales se almacenan en el registro en lugar que la RAM

una variable almacenada en registro
el maximo tamano de variable = maximo tamano de registro

el operador & no es posible porque no esta almacenada en RAM
sino en el registro

es un acceso mas rapido

comunmente usado es "counter"

memoria de registro != memoria RAM#include<stdio.h>

registro es la memoria integrada en el microprocesador
tiene poca capacidad, pero es de acceso mas rapido que a la ram
*/
#include<stdio.h>

int main()
{
int num1,num2;
register int sum;

printf("\nEnter the Number 1 : ");
scanf("%d",&num1);

printf("\nEnter the Number 2 : ");
scanf("%d",&num2);

sum = num1 + num2;

printf("\nSum of Numbers : %d\n",sum);

return(0);
}

// esta tecnica no se aplica para arrays structuras o punteros

/*

Keyword                 register
storage location        CPU register
initial value           Garbage ( el recolector de basura evalua si es char, int ...)
life                    local en el bloque donde fue declarada
scope                   bloque local
