
# creamos un metodo

# UseMethod()
# toma por parametro el nombre de la funcion generica

f <- function(x) UseMethod("f")

# otra manera

f.a <- function(x) "Class a"

# creo la clase
a <- structure(list(), class = "a")
class(a)

f(a)

# creo un metodo para la clase "a"

mean.a<-function(x) "a"
