f <- function() {
  x <- 1
  y <- 2
  c(x, y)
}
f()
rm(f) # lo quita del ambito(destructor)


#If a name isn’t defined inside a function, R will look one level up.

x <- 2
g <- function() {
  y <- 1
  c(x, y) # toma por default la variable con dicho nombre
          # en un ambito que engloba al actual
}
g()
rm(x, g)

