# Creamos un nuevo operador
# en este caso concatena strings

`%+%` <- function(a, b) paste0(a, b)
"new" %+% " string"

`%+%`("new", " string")
