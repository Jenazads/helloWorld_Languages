# medicion de tiempos de ejecucion

install.packages("lineprof")

library(lineprof)
# nos aoirta la funcion pause

f <- function() {
  pause(0.1)
  g()
  h()
}
g <- function() {
  pause(0.1)
  h()
}
h <- function() {
  pause(0.1)
}


lineprof(f())

# muestra los tiempos de ejecucion
