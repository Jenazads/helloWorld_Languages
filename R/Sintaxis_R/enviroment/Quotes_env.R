# la doble flecha <<- modifica una variable del entorno padre al actual

x <- 0
f <- function() {
  x <<- 1
}
f()
x

# si no exite tal variable, la crea 
# en esta funcion el entorno globabl es el GlobalEnv


