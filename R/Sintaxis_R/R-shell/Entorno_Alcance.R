# SCOPING RULES
# significa ambito o alcance 

# es el alcance de un namespaces
# R trabajacon namespaces
# se define un environment como un conjunto de pares
# simbolo valor (x<- 3.424)
# ahora solo existe un environment padre que es el vacio
# despues de el, los environment pueden tener hijos, y los hijos otros hijos
# cuando en R se define una variable, pasa al ambito del eviroment globoal
# y en el ambito de una funcion cuando hay variables libres
# R busca en todos y cada uno de los namespaces que hayan sido
# definidos o traidos mediante library() (otros paquetes)
# y cuando no encuentra lanza error, variable no definida


# R te permite definir funciones dentro de otras funciones
# puesto que el ambito de la funcion interna es 
# el ambito de la funcion que la contiene
# y el ambito del externo es el namespaces global
# y tambien una funcion puede devolver otra funcion !!!!

# ejemplo :

make.power<-function(n){
   pow<-function(x){
      x^n
   }
   pow
}

# esta funcion retorna la funcion pow() esperando el parametro "x"
# entonces escribimos
cube<-make.power(3)
# reemplaza a n=3 y espera el parametro "x"

cube(3)
# completa la funcion

# averiguando cual es el entorno de una funcion

ls(environment(cube))
# muestra lo que pertenece (o esta dentro) del enviroment cube

# extraemos un elemnto

get("n",environment(cube))

# si una funcion se define en el entorno global
# buscara sus parametros en el environment global

# si una funcion es definida dentro de otra funcion
# buscara sus parametros dentro del environment de la funcion
# que la contiene

# CONSECUENCIAS

# todos los objetos de R se almacenan en memoria

# cada funcion tiene un puntero hacia su respectivo
# environment de definicion (incluyendo las definidas dentro de otras)
# por lo que pueden estar en cualquier parte

# cuando es al environment global, no muestra la direccion de memoria
# cuando es funcion dentro de funcion, muestra la direccion de memoria (del environment) de la funcion contenedora


# OPTIMIZACION

# existen unas rutinas de optimizacion
# que requeriran como argumento una funcion con
# argumento a un vectoe de parametros.
# por ejemplo que quieras optimizar una funcino objetivo, maximizar o minimizar

optim # puede minimizar vectores de variables
nlm 
optimize # minimiza funciones de solo una variable

# las funciones de optimazion por defecto minimizan la funcion
# entonces si tu funcion objetivo esta disenada para ser maximizada
# debes tomar el negativo de optim,nlm,optimize
# para que puedan ser minimizadas (las funciones objetivos)

# ejemplo: se disena un constructor la cual
# me genera una probabilidad logaritmica negativa
# la cual quiero minimizar(funcion objetivo)

make.NegLogLik<-function(data,fixed=c(FALSE,FALSE)){
   params<-fixed
   function(p){
       params[!fixed]<-p
       mu<-params[1]
       sigma<-params[2]
       a<- -0.5*length(data)*log(2*pi*sigma^2)
       b<- -0.5*sum((data-mu)^2)/(sigma^2)
       -(a+b)
    }
}

# ahora hacemos

set.seed(1)
normals<-rnorm(100,1,2)

nLL<-make.NegLogLik(normals)
ls(environment(nLL))
# muestra lo que esta contenido en ese ambito
# pero las variables data , fixed y params son variables
# libres para la funcion con parametro "p"
# pero se definieron en el entorno de definicion (la funcion make.NegLogLik contenedora)

# Usando

# ahora usando optim sobre nLL

optim(c(mu=0,sigma=1),nLL)$par
# los parametros son:
# 1. los valores iniciales de los valores a optimizar
# (esto es debido a que solo definimos data  y no fixed)
# 2. funcion a ser minimizada

nLL<-make.NegLogLik(normals, c(FALSE,2))
optimize(nLL,c(-1,3))$minimum
# 1. los parametros son:
# la funcion a optimizar
# (porque los valores ya estan definidos en nLL)
# 2. el intervalo en donde se debe encontrar el minimo(optimizado)

# estima el valor de mu, porque mu fue puesto FALSE y luego se convierte en TRUE
