#!/usr/bin/env octave

#pkg load io;

function []=main(argsc,argsv)
  printf("\n\tHola Mundo M !!\n\n");
  printf("Numero de argumentos: %i\n",argsc+1);
  printf("Argumento[0] = %s\n",program_name());
  for i = 1:argsc
    printf("Argumento[%i] = %s\n",i,argsv{i});
  end
  return;
end

main(nargin,argv())

# run the file:
# octave helloWorld.m
