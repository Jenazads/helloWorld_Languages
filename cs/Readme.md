/* 

########################################################
###       @Copyright Moreno Vera, Felipe Adrian      ###
###                                                  ###
########################################################

## Instalar

      sudo apt-get install monodevelop mono-utils

0. PROGRAMA

// aun si esta en distinta carpeta

gmcs carpeta/archivo.cs

// y el compilado se genera en tu directorio actual

mono carpeta/archivo.exe

1. UN SOLO PROGRAMA

Para compilar un archivo con extension .cs en commandline se hacen lo siguiente:

gmcs (nombre_de_archivo).cs

// esto crea un ejecutable del compilado cs con extension .exe

// y luego ejecutamos el programa 

mono (nombre_de_archivo).exe

// y listo

*/
