#!/usr/bin/env ruby

#require 'io/console';

def main(argv)
  argc = argv.length;
  print("\n\tHola Mundo Ruby !!\n\n");
  print("Numero de argumentos: #{argc+1}\n");
  print("Argumento[0] = #{File.basename(__FILE__)}\n");
  for i in 0...argc
    print("Argumento[#{i+1}] = #{argv[i]}\n");
  end
  return;
end

main ARGV;

# run the file:
# ruby helloWorld.rb
