// Clases anónimas le permiten hacer su código más conciso. Ellos le permiten declarar y crear instancias de una clase a la vez.
// Son como las clases locales, con excepción de que no tienen un nombre. Úsalos si es necesario utilizar una clase local sólo una vez

// una clase anonima es clasificada como expresion

// una clase local son aquellas que estan definidias dentro de un metodo


// Clases anónimas le permiten hacer su código más conciso. Ellos le permiten declarar y crear instancias de una clase a la vez.
// Son como las clases locales, con excepción de que no tienen un nombre. Úsalos si es necesario utilizar una clase local sólo una vez

// una clase anonima es clasificada como expresion

// una clase local son aquellas que estan definidias dentro de un metodo

package loaq;

public class HelloWorldAnonymousClasses {
  
     interface HelloWorld {
         public void greet ();
         public void greetSomeone (String alguien);
     }
  
     public void sayHello () {
        // clase local
         class EnglishGreeting implements HelloWorld {
             String name = "mundo";
             @Override public void greet () {
                 greetSomeone ("mundo");
             }
             @Override public void greetSomeone (String alguien) {
                 name = alguien;
                 System.out.println ("Hola" + name);
             }
         }
      
         HelloWorld englishGreeting = new EnglishGreeting ();
        
        // clase anonymous
         HelloWorld frenchGreeting = new HelloWorld () {
             String name = "tout le monde";
             
             @Override  public void greet () {
                 greetSomeone ("tout le monde");
             }
             @Override public void greetSomeone (String alguien) {
                 name = alguien;
                 System.out.println ("Salut" + name);
             }
         };
        
        // clase anonymous
         HelloWorld spanishGreeting = new HelloWorld () {
             String name = "Mundo";
             @Override public void greet () {
                 greetSomeone ("Mundo");
             }
             @Override public void greetSomeone (String alguien) {
                 name = alguien;
                 System.out.println ("Hola" + name);
             }
         };
         englishGreeting.greet ();
         frenchGreeting.greetSomeone ("Fred");
         spanishGreeting.greet ();
     }

     public static void main (String ... args) {
         HelloWorldAnonymousClasses myApp =
             new HelloWorldAnonymousClasses ();
         myApp.sayHello ();
     }            
 }
/*
Al igual que las clases locales, clases anónimas pueden capturar las variables ; tienen el mismo acceso a las variables locales del ámbito circundante:

Una clase anónima tiene acceso a los miembros de su clase envolvente.

Una clase anónima no puede acceder a las variables locales en su ámbito circundante que no se declara como final o con eficacia final.

Al igual que una clase anidada, una declaración de un tipo (como una variable) en una clase anónima shadows( reemplaza ) cualquier otra declaración en el ámbito circundante que tienen el mismo nombre.
*/
