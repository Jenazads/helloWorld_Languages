class Clase {
  static int contador;

  Clase() {
   contador++;
  }

  static int getContador() {
   return contador;
  }
}


class Codigo {
  public static void main(String[] args) {
   Clase uno = new Clase();
   Clase dos = new Clase();
   Clase tres = new Clase();
   Clase cuatro = new Clase();
   System.out.println("Hemos declarado " + Clase.getContador() + " objetos.");
  }
}

// como getContador esta declarado como methodo static no es necesario declarar para llamarlo
