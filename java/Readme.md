/* 

########################################################
###       @Copyright Moreno Vera, Felipe Adrian      ###
###                                                  ###
########################################################

0. PROGRAMA

// aun si esta en distinta carpeta

javac carpeta/archivo.java

// y el compilado se genera en tu directorio actual

1. UN SOLO PROGRAMA

Para compilar un archivo con extension .java en commandline se hacen lo siguiente:

javac (nombre_de_archivo).java

// esto crea un codigo byte del compilado java con extension .class

// y luego ejecutamos el programa 

java (nombre_de_la_clase_main)
java nombre_archivo

// o si esta en alguna carpeta ( paquete )

javac paquete\(nombre_de_archivo).java

java paquete.nombre_archivo

// y listo

2. DOS O MAS PROGRAMAS en una misma carpeta

// tener todos los programas en una misma carpeta y compilar todos a la vez

javac (clase1).java (clase2).java (clase3).java ....

// y luego compilamos

java (nombre_clase_main)

DISTINAS CARPETAS/paquetes

javac fuente1.java paquete\fuente2.java paquete1\subpaquete\fuente3.java

// nos genera un archivo .class

// Y COMPILAMOS :::

java fuente1 paquete.fuente2 paquete.subpaquete.fuente3


PAQUETES 

se crea una carpeta 

├── modulo1.java
└── paquete 
    ├── modulo1.java 
    └── subpaquete  
        ├── modulo1.java
        └── modulo2.java
        
import paquete.modulo1;
import paquete.subpaquete.clase1;
import paquete.*;

todos los archivos en un paquete deben tener al inicio la siguiente linea

package ruta_paquete

// para modulo 1 y 2 del suboaquete

package paquete.subpaquete

*/
