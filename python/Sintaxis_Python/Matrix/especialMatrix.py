# matrices especiales
# matriz de vandermonde y de hilbert


def vandermode(v):
  n = len(v)
  a=[]
  b=[]
  for i in range(n):
    b=[]
    for j in range(n):
      b.append(float(v[j]**(n-i-1)))
    a.append(b)
  return a

def hilbert(n):
  a=[]
  b=[]
  for i in range(n):
    b=[]
    for j in range(n):
      b.append(1.0/(i+j+2-1))
    a.append(b)
  return a

def printMatriz(A):
  for i in range (0,len(A)):
    line = A[i]
    print(line)

printMatriz(hilbert(3))
