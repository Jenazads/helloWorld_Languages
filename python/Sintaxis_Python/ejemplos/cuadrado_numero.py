n = int(input("Ingreses el numero: "))

""" si queremos calcular el cuadrado de un numero "n"
     lo podemos expresar como 
     n^2 = 1 + 3 +5 + ... + (2*n - 1)
    estas 3 comillas tambien son un comentario pero de
    varias lineas
"""
suma = 0 # definimos para sumar.

for i in range(1,2*n): # esto de range nos da numeros de 1 a n
    if(i % 2 != 0):
        suma = suma + i
print "El cuadrado de %i es: %i " %(n,suma)
