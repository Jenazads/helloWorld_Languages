import random

# definimos las variables (asi se definen)

positivos, negativos, ceros =0, 0, 0
sumaposi, sumanega = 0, 0
promedio = 0
contador1 = 0
while(contador1 < 200):
    
	x = random.randint(-99,99);
	if(x>0):
	    sumaposi = sumaposi + x;
	    positivos = positivos + 1;
	elif(x<0):
	    sumanega = sumanega + x;
	    negativos = negativos + 1;
	else:
	    ceros = ceros + 1;
	promedio = promedio + x;
	print "El numero de lugar %i es: %i" %( contador1 +1 , x);
	contador1 = contador1 + 1;

promedio = promedio/200;

print "La suma de todos los numeros positivos es: " + str(sumaposi);

print "La suma de todos los numeros negativos es: " + str(sumanega);

print " La cantidad de numeros positivos es: " + str(positivos);

print "La cantidad de numeros negativos es: " + str(negativos);

print "La cantidad de Ceros es: " + str(ceros);

print "El promedio de todos los numeros es: " + str(promedio);
