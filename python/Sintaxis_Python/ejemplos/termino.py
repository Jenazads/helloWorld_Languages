i=1 # contador :D tu favorito
term=2 # primer termino
suma = 0 # suma los terminos

""" lo que quiero hacer es que
    2 ,      5 ,      7 ,    10 , ..... , 1800
    (1er)   (2do)    (3er)   (4to) ..... (n-simo numero)
    ese i = 1 , 2 , .... se refiere al numero de termino
    y para cuando i sea impar sumo 2 para irme al termino par
    y cuando i sea par sumo 3 para irme a un termino impar
"""

while(term <=1800):
    print term
    if(i % 2 == 0): # si el termino es par
        term = term + 2
    else: # si el termino es impar
        term = term + 3
    suma = suma + term # la suma de todos los terminos de la sucesion
    i+=1 # mi contador de numero de termino
    
print "La suma de todo es: %i " % suma


