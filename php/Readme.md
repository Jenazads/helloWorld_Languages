# Installing:

    sudo apt-get install php7.0

## Composer

Package Manager for php:

    sudo apt-get install composer
    composer install [package]

## Service

To serve any service just write:

    php artisan serve --port 800
    
## Programs

To run a php file:

    php file.php
