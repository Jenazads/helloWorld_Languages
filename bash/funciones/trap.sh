#!/bin/bash

# TRAP

# permite atrapar señales del sistema operativo

# hace que el programa termine lmpiamente( borrando datos temporales o ficheros usados )

cachado() {
    echo "Me has matado!!!"
    kill -15 $$
}
trap "cachado" 2 3
while true; do
    true
done 


# Señal 	Significado
# 0 	    salida del shell (por cualquier razón, incluido fin de fichero)
# 1 	    colgar
# 2 	    interrupción (Ctrl-C)
# 3 	    quit
# 9 	    kill (no puede ser parada ni ignorada)
# 15 	    terminate; señal por defecto generada por kill
