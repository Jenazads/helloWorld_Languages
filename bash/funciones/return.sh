#!/bin/bash

funcion2() {
    if [ -f /bin/ls -a -f /bin/ln ]; then
        return 0
    else
        return 1
    fi
}
# Programa principal
if funcion2; then
    echo "Los dos ficheros existen"
else
    echo "Falta uno de los ficheros - adiós"
    exit 1
fi
