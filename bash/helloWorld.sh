#!/usr/bin/env bash

#source "";

main() {
  printf "\n\tHola Mundo Bash !!\n\n";
  printf "Numero de argumentos: %i\n" "$(($#+1))";
  for((c=0; c<=("$#"); c++)); do  
    printf "Argumento[$c] = ${!c} \n";
  done
  return ;
}

main $@;

# Run the file:
# bash helloWorld.sh
