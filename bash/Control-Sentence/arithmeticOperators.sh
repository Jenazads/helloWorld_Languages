#!bin/bash

echo sp{el,il,al}l
# imprime spell spill pall

# Operator            Meaning

# VAR++, VAR--        Postincrement postdecrement
# ++VAS, --VAR        Preincrement predecrement
# !, ~                Logical and bitwise negation
# **                  Exponentiation
# *, /, %             mult, div, remainder
# +, -                addition, subtraction
# << >>               left and right bitwise shifts
# <=, >=, !=          comparison
# == !=               equal and inequality
# &                   bitwise AND
# ^              	    bitwise exclusive OR
# |	                  bitwise OR
# &&            	    logical AND
# ||            	    logical OR
# expr ? expr : expr	conditional evaluation
# =, *=, /=, %=, +=, -=, <<=, >>=, &=, ^= and |=	assignments
# ,                 	separator between expressions

# Al inicializar un numero, por default tomara 0

# si definimos "[BASE'#']N"
# donde BASE va de 2 a 64
# si BASE se omite, toma por default base 10

echo $[365*24]
# 8760

# opciones Shell

set -o



