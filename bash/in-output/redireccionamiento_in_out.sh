#!/bin/bash

# toda la E/S se hace a través de ficheros
# cada proceso tiene asociados 3 ficheros para la E/S 


# Nombre 	                Descriptor de fichero 	Destino por defecto
# entrada estándar (stdin) 	0 	                  teclado
# salida estándar (stdout) 	1 	                  pantalla
# error estándar (stderr) 	2 	                  pantalla

# por defecto, un proceso toma su entrada de la entrada estándar, envía su salida a la salida estándar y los mensajes de error a la salida de error estánda

# Carácter 	              Resultado
# comando < fichero 	    Toma la entrada de fichero
# comando > fichero 	    Envía la salida de comando a fichero; sobreescribe cualquier cosa de fichero
# comando 2> fichero 	    Envía la salida de error de comando a fichero (el 2 puede ser reemplazado por otro descriptor de fichero)
# comando >> fichero 	    Añade la salida de comando al final de fichero
# comando << etiqueta 	  Toma la entrada para comando de las siguientes lineas, hasta una línea que tiene sólo etiqueta
# comando 2>&1 	          Envía la salida de error a la salida estándar (el 1 y el 2 pueden ser reemplazado por otro descriptor de fichero, p.e. 1>&2)
# comando &> fichero 	    Envía la salida estándar y de error a fichero; equivale a comando > fichero 2>&1
# comando1 | comando2 	  Pasa la salida de comando1 a la entrada de comando2 (pipe)

# EJEMPLOS

#  ls -l > lista.ficheros
#  Crea el fichero lista.ficheros conteniendo la salida de ls -l

#  ls -l /etc >> lista.ficheros
#  Añade a lista.ficheros el contenido del directorio /etc

#  cat < lista.ficheros | more
#  Muestra el contenido de lista.ficheros página a página (equivale a more lista.ficheros)

#  ls /kaka 2> /dev/null
#  Envía los mensajes de error al dispositivo nulo (a la basura)

#  > kk
#  Crea el fichero kk vacío

#  cat > entrada
#  Lee información del teclado, hasta que se teclea Ctrl-D; copia todo al fichero entrada

#  cat << END > entrada
#  Lee información del teclado, hasta que se introduce una línea con END; copia todo al fichero entrada

#  ls -l /bin/bash /kaka > salida 2> error
#  Redirige la salida estándar al fichero salida y la salida de error al fichero error

#  ls -l /bin/bash /kaka > salida.y.error 2>&1
#  Redirige la salida estándar y de error al fichero salida.y.error; el orden es importante:

#  ls -l /bin/bash /kaka 2>&1 > salida.y.error 
#  no funciona, por qué?

#  ls -l /bin/bash /kaka &> salida.y.error
#  Igual que el anterior

#  cat /etc/passwd > /dev/tty2
#  Muestra el contenido de /etc/passwd en el terminal tty2
#  usar el comando tty para ver el nombre del terminal en el que estamos


 Comandos útiles con pipes y redirecciones

  tee
    copia la entrada estándar a la salida estándar y también al fichero indicado como argumento:
      ls -l | tee lista.ficheros | less
      Muestra la salida de ls -l página a página y la almacena en lista.ficheros
    Opciones:
       -a: no sobreescribe el fichero, añade al final
  xargs
     permite pasar un elevado número de argumentos a otros comandos
     lee la entrada estándar, y ejecuta el comando uno o más veces, tomando como argumentos la entrada estándar (ignorando líneas en blanco)
      Ejemplos:
         $ locate README | xargs cat | fmt -60 >\
           /home/pepe/readmes
         locate encuentra los ficheros README; mediante xargs los ficheros se envían a cat que muestra su contenido; este se formatea a 60 caracteres por fila con fmt y se envía al fichero readmes

         $ locate README | xargs -i cp {} /tmp/
           copia los README en el directorio /tmp; la opción -i permite que {} sea reemplazado por los nombres de los ficheros 

  exec
     ejecuta un programa reemplazando el shell actual con el programa (es decir, al programa se le asigna el PID del shell, dejando el shell de existir)

      $ echo $$ #$$ indica el PID del shell actual
      4946
      $ exec sleep 20
      En otro terminal, ejecutamos
      $ ps a | grep 4946
       4946 pts/13     Ss+     0:00 sleep 20 

      si no se especifica el programa, exec puede usarse para redireccionar las entradas y salidas
        Redirecciona la salida estándar a el fichero /tmp/salida

        $ exec > /tmp/salida 

         Redirecciona el fichero /tmp/entrada como entrada estándar

           $ exec < /tmp/entrada 
