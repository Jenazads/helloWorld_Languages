#!/bin/bash

# SELECT selecciona literalmente uno de las opciones
# es un bucle hasta que se seleccione uno
# se selecciona escrbiendo le numero de posicion
# ejemplo Empezar = 1

 select ACCION in Empezar Repetir Acabar
 do
  case $ACCION in

  "Empezar")
    echo "El usuario quiere empezar"
   ;;
  "Repetir")
    echo "El usuario quiere repetir"
   ;;
  "Acabar")
    echo "El usuario quiere salir"
    break
   ;;
  *)
    echo "No se que quiere el usuario"
   ;;
  esac
 done
