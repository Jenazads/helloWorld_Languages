#!/bin/bash

# Permite definir variables cuyo nombres son contenido de otra
# variable

a=letra
letra=z
# Referencia directa
echo "a = $a"    # a = letra
# Referencia indirecta
eval a=\$$a
echo "Ahora a = $a"    # Ahora a = z

# a partir de BAsh version 2

a=letra
letra=z
# Referencia directa
echo "a = $a"    # a = letra
# Referencia indirecta
echo "Ahora a = ${!a}"    # Ahora a = z

# EJEMPLO

dniPepe=23456789
dniPaco=98765431
echo -n "Nombre: "; read nombre
eval echo "DNI = \$dni${nombre}"


