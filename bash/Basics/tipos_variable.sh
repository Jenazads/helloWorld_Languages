#!/bin/bash

#Tipos de variable

# Asignar un valor: nombre_variable=valor

un_numero=15
nombre="Pepe Pota" 

# Acceder a las variables: ${nombre_variable} o $nombre_variable

echo $nombre
echo ${nombre}

# Eliminar una variable: unser nombre_variable

unset nombre
echo ${nombre}mo

# Variables de solo lectura

readonly nombre
unset nombre
echo $?
# botara error, esdecir, un numero distinto de 0


# Nombre 	    Propósito
# HOME 	        directorio base del usuario
# SHELL 	      shell por defecto
# USERNAME 	    el nombre de usuario
# PWD 	        el directorio actual
# PATH 	        el path para los ejecutables
# MANPATH 	    el path para las páginas de manual
# PS1/PS2 	    prompts primario y secundario
# LANG 	        aspectos de localización geográfica e idioma
# LC_* 	        aspectos particulares de loc. geográfica e idioma

source
env
