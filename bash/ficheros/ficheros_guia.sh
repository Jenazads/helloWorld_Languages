#!/bin/bash

# GUIA DE FICHEROS
              VERDADERO SI:
-e file 	      file existe
-r file 	      file existe y es legible
-w file 	      file existe y se puede escribir
-x file 	      file existe y es ejecutable
-f file 	      file existe y es de tipo regular
-d file 	      file existe y es un directorio
-c file 	      file existe y es un dispositivo de caracteres
-b file 	      file existe y es un dispositivo de bloques
-p file 	      file existe y es un pipe
-S file 	      file existe y es un socket
-L file 	      file existe y es un enlace simbólico
-u file 	      file existe y es setuid
-g file 	      file existe y es setgid
-k file 	      file existe y tiene activo el sticky bit
-s file 	      file existe y tiene tamaño mayor que 0
file1 -nt file2	file1 mas actual(según la fecha de modificación) que file2.
file1 -ot file2	file1 mas antiguo que file2.
file1 -ef file2	file1 y file2 tiene el mismo numero de device e inodo.

